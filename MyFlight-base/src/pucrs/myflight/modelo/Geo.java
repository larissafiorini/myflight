package pucrs.myflight.modelo;

import java.lang.Math;

public class Geo {
	private double latitude;
	private double longitude;

	public Geo(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public static double distancia(Geo g1, Geo g2) {
		int r = 6371;
		double d = 0;
		double latitude1 = Math.toRadians(g1.latitude);
		double latitude2 = Math.toRadians(g2.latitude);
		double lat = (latitude1 - latitude2) / 2;
		double longit = Math.toRadians((g1.longitude - g2.longitude) / 2);
		double quad1 = Math.pow(Math.sin(lat), 2.0);
		double quad2 = Math.pow(Math.sin(longit), 2.0);

		d = 2*r * Math.asin(Math.sqrt(quad1 + quad2 * Math.cos(latitude1) * Math.cos(latitude2)));

		return d;
	}
}
