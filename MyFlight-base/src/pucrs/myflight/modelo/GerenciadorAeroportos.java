package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorAeroportos {
	private ArrayList<Aeroporto> aeroportos;
	
	public GerenciadorAeroportos() {
		aeroportos = new ArrayList<>();
	}
	public ArrayList<Aeroporto> listaTodos() {
		ArrayList<Aeroporto> aux = new ArrayList<>();
		for(Aeroporto aerop:aeroportos)
			aux.add(aerop);
		return aux;
	}
	public void adicionar(Aeroporto aero){
        aeroportos.add(aero);
    }
    public Aeroporto buscarPorCodigo(String cod) {
        int c = 0;
        while (c < aeroportos.size()) {
            Aeroporto ae1 = aeroportos.get(c);
            if (ae1.getCodigo() == cod) {
                break;
            }
        }
        return aeroportos.get(c);

    }
}