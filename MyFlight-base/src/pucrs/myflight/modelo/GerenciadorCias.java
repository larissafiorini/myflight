package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorCias {
	private ArrayList<CiaAerea> empresas;
	
	public GerenciadorCias() {
		empresas = new ArrayList<>();
	}
	
	public ArrayList<CiaAerea> listaTodas() {
		ArrayList<CiaAerea> aux = new ArrayList<>();
		for(CiaAerea cia:empresas)
			aux.add(cia);
		return aux;
	}
	public void adicionar(CiaAerea cia) {
        empresas.add(cia);
    }

    public CiaAerea buscarPorCodigo(String cod) {
        int c = 0;
        while (c < empresas.size()) {
            CiaAerea ca1 = empresas.get(c);
            if (ca1.getCodigo() == cod) {
                break;
            }
        }
        return empresas.get(c);

    }
    
    public CiaAerea buscarNome(String cod) {
        int c = 0;
        while (c < empresas.size()) {
            CiaAerea ca1 = empresas.get(c);
            if (ca1.getNome().equalsIgnoreCase(cod)) {
                break;
            }
        }
        return empresas.get(c);
    }
}
