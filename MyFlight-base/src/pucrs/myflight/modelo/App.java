package pucrs.myflight.modelo;

import java.time.LocalDateTime;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

public class App {

	public static void main(String[] args) {

		CiaAerea gol = new CiaAerea("G3", "Gol Linhas A�reas SA");
		CiaAerea latam = new CiaAerea("JJ", "LATAM Linhas A�reas");

		GerenciadorCias gerCias = new GerenciadorCias();
		gerCias.adicionar(gol);
		gerCias.adicionar(latam);

		Geo locPoa = new Geo(-29.9939, -51.1711);
		Aeroporto poa = new Aeroporto("POA", "Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711));
		Aeroporto gru = new Aeroporto("GRU", "S�o Paulo Guarulhos Intl Apt", new Geo(-23.4356, -46.4731));

		GerenciadorAeroportos gerAero = new GerenciadorAeroportos();
		gerAero.adicionar(poa);
		gerAero.adicionar(gru);

		GerenciadorAeronaves gerAvioes = new GerenciadorAeronaves();
		gerAvioes.adicionar(new Aeronave("733", "Boeing 737-300"));
		gerAvioes.adicionar(new Aeronave("73G", "Boeing 737-700"));

		GerenciadorRotas gerRotas = new GerenciadorRotas();
		gerRotas.adicionar(new Rota(gol, gru, poa, gerAvioes.buscarCodigo("733")));
		gerRotas.adicionar(new Rota(gol, poa, gru, gerAvioes.buscarCodigo("73G")));

		ArrayList<Rota> rotas = gerRotas.listarTodas();
		GerenciadorVoos gerVoos = new GerenciadorVoos();
		

		// V�o com dura��o de 1h30min GRU->POA
		LocalDateTime datahora1 = LocalDateTime.of(2016, 8, 18, 8, 30);
		Duration duracao1 = Duration.ofMinutes(90);
		Duration duracao2 = Duration.ofMinutes(120);
		// Rota: GRU -> POA
		Rota rota1 = rotas.get(0);
		Rota rota2 = rotas.get(1);
		//Voo voo1 = new Voo(rota1, datahora1, duracao1);
		//Voo voo2 = new Voo(rota2, datahora1, duracao2);
		
		
		// N�o � mais poss�vel criar objetos Voo (classe abstrata)
		//Voo voo1 = new Voo(rota1,datahora1, duracao1);
		
		VooDireto voo1 = new VooDireto(datahora1, rota1);
		VooEscalas voo2 = new VooEscalas(LocalDateTime.of(2016,9,11,8,0));
		voo2.adicionarRota(rota2);
		voo2.adicionarRota(rota3);
		
		gerVoos.adicionar(voo1);
		gerVoos.adicionar(voo2);

		
		ArrayList<Voo> listaVoos = gerVoos.listarTodos();
		for(Voo v: listaVoos) {			
			System.out.println("Voo: "+v);
			if(v instanceof VooEscalas) {
				VooEscalas v2 = (VooEscalas) v;
				// Exemplo: acessa lista de rotas do objeto v
				System.out.println(v2.getRotas());
			}
		}
		// Teste: procurar o v�o
		ArrayList<Voo> meusVoos = gerVoos.buscarData(LocalDate.of(2016, 8, 18));
		for (Voo v : meusVoos) {
			System.out.println(v.getRota().getOrigem().getNome());
			System.out.println(v.getRota().getDestino().getNome());
			System.out.println(v.getDatahora());
		}
		//gerRotas.ordenaCia();
		gerRotas.ordenaOrigem();
	//	gerRotas.ordenarOrigemCia();
	
		// double d = Geo.distancia(locPoa, geo2);
		/*
		 * System.out.print("Dist�ncia= "+ d);
		 * 
		 * //gerAvioes.ordenarAeronaves(); gerAvioes.ordenarDescricao();
		 */

		for (Rota r : rotas)
			System.out
					.println(r.getCia().getNome() + "-" + r.getOrigem().getNome() + " -> " + r.getDestino().getNome());

		//gerVoos.ordenaDataHora();
		gerVoos.ordenaDataHoraDuracao();
		for (Voo v : meusVoos) {
			System.out.println(v.getRota().getOrigem().getNome());
			System.out.println(v.getRota().getDestino().getNome());
			System.out.println(v.getDatahora());
		}
		Rota rota3 =rotas.get(1);
	//	VooEscalas voo3 = new VooEscalas(rota3, rota2, LocalDateTime.of(2016,9,11,8,0), Duration.ofMinutes(14*60));
		gerVoos.adicionar(voo1);
		//gerVoos.adicionar(voo3);
		System.out.println(rotas.toString());
	}
}